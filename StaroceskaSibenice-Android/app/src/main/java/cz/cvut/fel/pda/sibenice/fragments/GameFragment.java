package cz.cvut.fel.pda.sibenice.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.cvut.fel.pda.sibenice.R;

/**
 * Created by Daniil on 23.11.2014.
 */
public class GameFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.game_fragment, container, false);
        return view;
    }
}

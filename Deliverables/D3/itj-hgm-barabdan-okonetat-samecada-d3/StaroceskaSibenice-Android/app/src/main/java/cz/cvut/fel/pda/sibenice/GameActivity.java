package cz.cvut.fel.pda.sibenice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Daniil on 23.11.2014.
 */
public class GameActivity extends Activity {

    private AlertDialog.Builder alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_game_screen);

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.end_game_message_popup, null);
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setView(dialoglayout);

        dialoglayout.findViewById(R.id.back_to_main_menu_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alertDialog.setCancelable(false);

        // TODO: presunout do strings.xml !
        String msg = "Tabulka výsledků";
        alertDialog.setMessage(msg.subSequence(0,msg.length()));

        // TODO: nemazat! mozna se to bude pak hodit...
//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }
    }

    public void doShowResults(View view) {
        alertDialog.show();
    }
}

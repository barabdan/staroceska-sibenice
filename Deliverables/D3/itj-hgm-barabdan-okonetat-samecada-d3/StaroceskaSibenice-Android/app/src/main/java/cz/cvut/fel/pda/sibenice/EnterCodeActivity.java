package cz.cvut.fel.pda.sibenice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Daniil on 23.11.2014.
 */
public class EnterCodeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // zavolame construktor predka
        super.onCreate(savedInstanceState);

        // zapneme hlavni layout, ktere jsme definovali v .xml
        setContentView(R.layout.enter_group_code_screen);

//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment()).commit();
//        }
    }

    // zpracovani udalosti na klik tlacitka "Pripojit se"
    // otevre se nova obrazovka s klavesnici, chatem a seznamem hracu
    public void doStartGame(View view) {
        // zalozime novou aktivitu a nastartujeme ji
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() { }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater,
//                                 ViewGroup container,
//                                 Bundle savedInstanceState) {
//            // ...
//            View rootView = inflater.inflate( R.layout.enter_group_code_screen,
//                                              container, false );
//            return rootView;
//        }
//    }
}
